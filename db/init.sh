#!/bin/bash

ROOT_DIR=$(cd $(dirname $0)/..; pwd)
DB_DIR="$ROOT_DIR/db"
BENCH_DIR="$ROOT_DIR/bench"
MYSQL_HOST=is2

export MYSQL_PWD=isucon

mysql -h $MYSQL_HOST -uisucon -e "DROP DATABASE IF EXISTS torb; CREATE DATABASE torb;"
mysql -h $MYSQL_HOST -uisucon torb < "$DB_DIR/schema.sql"

gzip -dc "$DB_DIR/b.sql.gz" | mysql -h $MYSQL_HOST -uisucon torb
