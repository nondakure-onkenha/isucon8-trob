#!/bin/bash -eu

APP_HOST="is1 is3"

for h in $APP_HOST;
do
    echo $h
    ssh $h "
cd isucon8-trob
git pull
sudo systemctl daemon-reload
sudo systemctl restart trob.python.service
systemctl status trob.python.service
"
done;
